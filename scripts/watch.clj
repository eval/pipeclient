(require '[cljs.build.api :as b])

(b/watch "src"
  {:main 'pipeclient.core
   :output-to "out/pipeclient.js"
   :output-dir "out"})
