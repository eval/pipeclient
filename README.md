# pipeclient

Terminal dashboard for GitLab pipelines.

![](assets/screenshot.png)

## Usage

Requirements:

* [access token](https://gitlab.com/profile/personal_access_tokens)

```bash
$ npm install -g pipeclient
# from project-folder that is hosted on GitLab:
$ export PC_GITLAB_TOKEN=my-token
$ pipeclient
```

Other environment variables:

* `PC_GIT_REMOTE`: set remote that points to (your install of) GitLab. Default: `origin`.


## Development

```bash
$ lein cljsbuild once

# run from project-folder that is hosted on gitlab.com
$ export PC_GITLAB_TOKEN=my-token
$ node main.js
```

## License

Copyright © 2016 Gert Goet, ThinkCreate.

Distributed under the terms of the MIT License.

