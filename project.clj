(defproject pipeclient "0.1.0-SNAPSHOT"
  :description "Terminal dashboard for GitLab pipelines"

  :url "https://gitlab.com/eval/pipeclient"

  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/clojurescript "1.9.293"]
                 [clojure-humanize "0.2.2"]
                 [lein-doo "0.1.7"]
                 [org.clojure/core.async "0.2.395"]]

  :jvm-opts ^:replace ["-Xmx1g" "-server"]

  :plugins [[lein-cljsbuild "1.1.5"]
            [lein-doo "0.1.7"]]

  :source-paths ["src" "test" "target/classes"]

  :clean-targets ["out" "release" "pipeclient.js" "pipeclient-test.js" "pipeclient-dev.js"]

  :cljsbuild {
    :builds [{:id "dev"
              :source-paths ["src"]
              :compiler {
                :output-to "pipeclient-dev.js"
                :output-dir "out"
                :target :nodejs
                :verbose true
                :optimizations :none
                :source-map false}}
              {:id "prod"
               :source-paths ["src"]
               :compiler {
                :output-to "pipeclient.js"
                :main       pipeclient.core
                :target :nodejs
                :optimizations :simple}}
              {:id "test"
               :source-paths ["src" "test"]
               :compiler {
                :output-to "pipeclient-test.js"
                :main       pipeclient.runner
                :target :nodejs
                :optimizations :simple}}]})
