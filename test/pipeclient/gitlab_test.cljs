(ns pipeclient.gitlab-test
  (:require [cljs.test :refer-macros [async deftest is testing are use-fixtures]]
            [pipeclient.gitlab :as gitlab :refer [GitLabConfig]]))

(deftest base-url
  (is (= "https://gitlab.com/api/v3" (gitlab/base-url {:host "gitlab.com"})))
  (is (= "https://my-host.com/api/v3" (gitlab/base-url {:host "my-host.com"})))
)

(deftest url-generator
  (are [template params result] (= result (apply (gitlab/url-generator template) (-> params seq flatten)))
      "index.html" {:a 1}                     "index.html?a=1"
      "index.html" {:a 1 :b 2}                "index.html?b=2&a=1"
      "/project/:name" {:name "hello" :b 2}   "/project/hello?b=2"
      "/project/:name" {:name "hello"}        "/project/hello"
))

(deftest project-url
  (is (= "https://my.com/api/v3/projects/eval%2Fmy-project" (gitlab/project-url {:host "my.com" :project "eval/my-project"}))))

(deftest urls-pipelines
  (are [config result] (= result ((gitlab/urls config :pipelines) :per_page 20))
       {:host "gitlab.com" :project "eval/pipeclient"} "https://gitlab.com/api/v3/projects/eval%2Fpipeclient/pipelines?per_page=20"
       {:host "my.com" :project "eval/project"} "https://my.com/api/v3/projects/eval%2Fproject/pipelines?per_page=20"
))

(deftest urls-commit
  (are [config result] (= result ((gitlab/urls config :commit) :sha "sha-123"))
       {:host "gitlab.com" :project "eval/pipeclient"} "https://gitlab.com/api/v3/projects/eval%2Fpipeclient/repository/commits/sha-123"
))

(deftest remote-url->host
  (are [url result] (= result (gitlab/remote-url->host url))
        "git@gitlab.com:eval/pipeclient.git"          "gitlab.com"
        "https://gitlab.com/eval/pipeclient.git"      "gitlab.com"
        "git@my-own-host.com:eval/pipeclient.git"     "my-own-host.com"
        "https://my-own-host.com/eval/pipeclient.git" "my-own-host.com"
))

(deftest remote-url->project
  (are [url result] (= result (gitlab/remote-url->project url))
        "git@gitlab.com:eval/pipeclient.git"          "eval/pipeclient"
        "https://gitlab.com/eval/pipeclient.git"      "eval/pipeclient"
        "git@gitlab.com:some-group/my-project.git"     "some-group/my-project"
))
