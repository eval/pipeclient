(ns pipeclient.runner
  (:require [cljs.test :as test]
            [doo.runner :refer-macros [doo-all-tests doo-tests]]
            [pipeclient.util-test]
            [pipeclient.gitlab-test]
          ))

(doo-tests 'pipeclient.util-test
           'pipeclient.gitlab-test
)
