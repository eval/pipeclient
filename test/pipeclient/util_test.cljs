(ns pipeclient.util-test
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [cljs.test :refer-macros [async deftest is testing are]]
            [pipeclient.util :as util]
            [cljs.core.async :refer [put! take! chan <! >! timeout] :as async]))

(deftest format-duration
  (are [count expected] (= expected (util/format-duration count))
        2    "0:02"
        10   "0:10"
        60   "1:00"
        65   "1:05"
        600  "10:00"
))

(deftest dimensions
  (are [grid dim] (= dim (util/dimensions grid))
       []           {:rows 0 :cols 0}
       [[]]         {:rows 1 :cols 0}
       [[:a]
        [:b]]       {:rows 2 :cols 1}
       [[:a :b]]    {:rows 1 :cols 2}
))

(deftest grid-widgets-positions
  (defn positions-only [gw]
    (reduce (fn [res [k v]] (assoc res k (first v))) {} gw))

  (are [grid widgets] (= widgets (positions-only (util/grid-widgets grid)))
        [[:a :a]]   {:a [0 0]}
        [[:a]
         [:b]]      {:a [0 0] :b [1 0]}
        [[:a :a]
         [:b :b]]   {:a [0 0] :b [1 0]}
))

(deftest grid-widgets-sizes
  (defn sizes-only [gw]
    (reduce (fn [res [k v]] (assoc res k (last v))) {} gw)
    )

  (are [grid widgets] (= widgets (sizes-only (util/grid-widgets grid)))
        [[:a :a]]   {:a [1 2]}
        [[:a]
         [:b]]      {:a [1 1] :b [1 1]}
        [[:a :a]
         [:b :b]]   {:a [1 2] :b [1 2]}
        [[:a   :a]
          [nil :b]] {:a [1 2] :b [1 1]}  ;; nil is filtered out
        [[:a   :b]
         [:a  nil]] {:a [2 1] :b [1 1]}
        [[:a   :a  :b]
         [:a   :a nil]] {:a [2 2] :b [1 1]}
))
