(ns pipeclient.util
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [cljs.core.async :refer [put! take! chan <! >! timeout close!] :as async]
            [clojure.walk :refer [keywordize-keys]]
            [clojure.string :as str]
            [goog.string :as gstr]
            [goog.string.format]

))

(defn spy [x]
  (prn x)
  x)

;; source: http://www.lispcast.com/core-async-code-style
(defn <<< [f & args]
  (let [c (chan)]
    (apply f (concat args [(fn [x]
                             (if (or (nil? x)
                                     (undefined? x))
                               (close! c)
                               (put! c x)))]))
    c))

(defn format-duration [count]
  (let [min (quot count 60)
        sec (rem count 60)]
        (gstr/format "%01d:%02d" min sec)
))


(defn parse-json [s]
  (keywordize-keys (js->clj (.parse js/JSON s))))

(defn dimensions [[row1 :as grid]]
  "[[:cell1] [:cell2]] => {:rows 2 :cols 1}"
  {:rows (count grid) :cols (count row1)})

(defn grid-widgets [grid]
  "[[:a :b] [:a :b]] => {:a [[0 0] [2 1]] :b [[0 1] [2 1]]}"
  (let [{:keys [rows cols]} (dimensions grid)
        coords (for [y (range rows) x (range cols)] [y x])  ;; ([0 0] [0 1] [1 0] [1 1])
        cells  (for [rows grid cell rows] cell)             ;; '(:a :b :a :b)
        cell-coord (map-indexed (fn [ix cell] [cell (nth coords ix)]) cells)] ;; ([:a [0 0]] [:b [0 1]])
    (reduce (fn [res [cell coord]]
              (if (nil? cell)
                res
                (let [[existing-coord _] (get res cell [coord []])]
                  (assoc res cell (conj [existing-coord] (mapv + [1 1] (mapv - coord existing-coord)))))))
              {}
              cell-coord))
)
