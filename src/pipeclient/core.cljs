(ns pipeclient.core
  (:require-macros [cljs.core.async.macros :refer [go go-loop]])
  (:require [clojure.contrib.humanize :refer [truncate datetime] :as hum]
            [cljs.core.async :refer [put! take! chan <! >! timeout] :as async]
            [clojure.string :as str]
            [cljs.nodejs :as node]
            [pipeclient.gitlab :as gl :refer [GitLabConfig]]
            [pipeclient.util :as util :refer [<<<]]))

(enable-console-print!)

(def blessed  (js/require "blessed"))
(def contrib  (js/require "blessed-contrib"))
(def colors   (js/require "colors/safe"))
(def chps     (js/require "child_process"))

(def app-state (atom {:pipelines [] :commits {} :logging []}))

(def events (chan))

(def status-colors {  :running colors.blue
                      :success colors.green
                      :failed  colors.red
                      :skipped colors.grey
                      :pending colors.yellow
                      :canceled colors.cyan
                      :default colors.yellow
             })

(defn enqueue [ch data]
  (go
    (>! ch data)))

(defn log [line]
  (enqueue events [:logging [line]]))

(defn pipeline-row [commits {status :status :keys [ref sha started_at created_at duration]}]
  (let [color (get status-colors (keyword status) (:default status-colors))
        dur   (or duration (and started_at (/ (- (js/Date.) (js/Date. started_at)) 1000)))
        sha-short (subs sha 0 6)
        commit-msg (or (some-> (get commits sha) :message str/split-lines first) "...")
        relative-date (datetime (js/Date. created_at))]
    (map color [(truncate ref 35) (str sha-short " - " (truncate commit-msg 45)) (util/format-duration dur) relative-date])))

(defn pipelines-widget []
  (let [elm       contrib.table
        config    (clj->js {:columnWidth [40 60 10 20] :columnSpacing 0 :label "Pipelines" :interactive true :keys true})
        renderer  (fn [elm data]
                    (.focus elm)
                    (let [commits (:commits data)
                          table-data (map #(pipeline-row commits %) (:pipelines data))]
                      (.setData elm (clj->js {:headers '("ref" "commit" "duration" "time") :data table-data}))))]
    [elm config renderer]
))

(defn log-widget []
  (let [elm contrib.log
        config (clj->js {:label "Log"})
        renderer (fn [elm state]
                  (set! (.-logLines elm) #js [])
                  (doseq [[ts line] (:logging state)]
                    (.log elm (str/join " " [(.toISOString ts) line]))))]
    [elm config renderer]))

(defn exit [& [msg]]
  (when msg (println msg))
  (.exit node/process 0))

(defn set-events [screen]
  (do
    (.key screen #js ["escape" "q" "C-c"] exit)))

(def grid-cfg [[pipelines-widget]])

(defn handle-events! []
  (go-loop []
    (let [[event & [data]] (<! events)]
      (condp = event
        :logging-disabled (swap! app-state update-in [:logging] conj (into [(js/Date.)] data))
        :pipelines (swap! app-state merge {:pipelines data})
        :gl-commit (let [[sha commit] data]
                    (swap! app-state update-in [:commits] assoc sha commit))
        :no-op
))
       (recur)))

(defn init-screen []
  (let [screen (.screen blessed #js {:smartCSR true})
        grid (contrib.grid. (clj->js (assoc (util/dimensions grid-cfg) :screen screen)))]
    (doseq [[widget [coord size]] (util/grid-widgets grid-cfg)]
      (let [[elm cfg renderer] (widget)
            args (conj (into coord size) elm cfg)
            obj (.apply (.-set grid) grid (to-array args))]
            (renderer obj @app-state)
            (add-watch app-state obj #(do (renderer obj %4) (.render screen)))))
    (set-events screen)
    (handle-events!)))

(defn unknown-commits
  "Commits found in pipelines collection but not in commits"
  []
  (let [known-shas (set (keys (:commits @app-state)))]
    (distinct (remove known-shas (map :sha (:pipelines @app-state))))))

(defn gitlab-callback-for-status [status cb]
  (fn [err resp body] (when (and resp (= (.-statusCode resp) status)) (cb body))))

(defn gitlab-fetcher [config]
  (go-loop []
    (gl/pipelines config
                  (gitlab-callback-for-status 200
                    #(enqueue events [:pipelines (util/parse-json %1)])))

    (doseq [sha (take 2 (unknown-commits))]
      (gl/commit config sha
                (gitlab-callback-for-status 200
                  #(let [{:keys [id] :as commit} (util/parse-json %1)]
                        (enqueue events [:gl-commit [id commit]])))))

    (<! (timeout 5e3))

    (recur)))

(defn git-remote-url [{:keys [remote] :or {remote "origin"}} cb]
  (.exec chps (str "git config --get remote." remote ".url") #(cb %2)))

(defn gitlab-config [& {:keys [remote-url env]}]
  "Yields a GitLabConfig"
  (let [project (gl/remote-url->project remote-url)
        host    (gl/remote-url->host remote-url)
        token   (aget env "PC_GITLAB_TOKEN")]
    (GitLabConfig. token host project)))

(defn -main []
  (log "Starting main")
  (go
    (let [git-remote (or node/process.env.PC_GIT_REMOTE "origin")
          config (gitlab-config :remote-url (<! (<<< git-remote-url {:remote git-remote})) :env node/process.env)]
      (if (<! (<<< gl/authenticated? config))
        (do (init-screen)
            (gitlab-fetcher config))
        (exit "Authentication failed. Please check the value of $PC_GITLAB_TOKEN.")))))

(set! *main-cli-fn* -main)
