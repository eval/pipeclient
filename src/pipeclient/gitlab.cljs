(ns pipeclient.gitlab
  (:require [clojure.string :as str]
            [cljs.nodejs :as node]
            [pipeclient.util :as util]))

(def req (node/require "request"))
(def qs (node/require "querystring"))

(defrecord GitLabConfig [token host project])

(defn remote-url->host [remote-url]
  (let [ssh-re    #"@([^:]+)"
        https-re  #"https://([^/]+)"]
    (second (some #(re-find % remote-url) [https-re ssh-re]))))

(defn remote-url->project [remote-url]
  (let [ssh-re    #":([^.]+)"
        https-re  #"://[^/]+/([^.]+)"]
    (second (some #(re-find % remote-url) [https-re ssh-re]))))

(defn base-url [{:keys [host]}]
  (str "https://" host "/api/v3"))

(defn project-url [{:keys [host project] :as config}]
  (let [encoded-project (str/replace project \/ "%2F")]
    (str (base-url config) "/projects/" encoded-project)))

(defn url-generator [url]
  "Yields function that generates urls:
    ((url-generator \"/projects\") :sort \"asc\") => \"/projects?sort=asc\"
  url may contain placeholders:
    ((url-generator \"/projects/:id\") :id 1 :sort \"asc\") => \"/projects/1?sort=asc\""
  (fn [& {:as params}]
    (let [non-query-keys (map (fn [[a b]] [a (keyword b)]) (re-seq #":(\w+)" url))
          queryable-params (select-keys params (remove (set (map last non-query-keys)) (keys params)))
          template (reduce
                     (fn [res [token key]]
                      (str/replace res token (get params key))) url non-query-keys)
          query (.stringify qs (clj->js queryable-params))]
      (str/join "?" (remove str/blank? [template query])))))

(defn urls [config id]
  (let [purl (project-url config)]
    (condp = id
      :pipelines  (url-generator (str purl "/pipelines"))
      :commit     (url-generator (str purl "/repository/commits/:sha")))))

(defn get-request [{:keys [token]} {:keys [url]} cb]
  (req (clj->js {:url url :headers {"PRIVATE-TOKEN" token}}) cb))

(defn pipelines [config cb]
  (let [url ((urls config :pipelines) :per_page 20)]
    (get-request config {:url url} cb)))

(defn commit [config sha cb]
  (let [url ((urls config :commit) :sha sha)]
    (get-request config {:url url} cb)))

(defn authenticated? [config cb]
  (pipelines config (fn [err resp body] (cb (not= 401 (.-statusCode resp))))))
